from fastapi import FastAPI, Request, HTTPException, Query
import pymongo
from mongoengine import connect
import motor.motor_asyncio
from bson.objectid import ObjectId
from api.routers import job

app = FastAPI()
# connect(db='hrms', host='localhost', port=27017)

app.include_router(job.router)



# def get_mongo_conn():
#     MONGO_DETAILS = (
#         f"mongodb://{config.MONGO_DATA_USER}:{config.MONGO_DATA_PASSWORD}"
#         f"@{config.MONGO_DATA_HOST}:{config.MONGO_DATA_PORT}/{config.MONGO_DATA_DATABASE}?authSource=admin&retryWrites=true&w=majority"
#     )
#     client = MongoClient(MONGO_DETAILS)
#     db = client.jobdata
#     engineerdata_collection = db.EngineerData
#     return engineerdata_collection




# @app.get("/job_detail")
# async def job_detail(JobKeyword: str):
#     results = []
#     print(JobKeyword.lower())
#     engineerdata_collection = get_mongo_conn()
#     for result in engineerdata_collection.find({"JobKeyword": JobKeyword.lower()}):
#         results.append(database.engineer_helper(result))
#     return results
