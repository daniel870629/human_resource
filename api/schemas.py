import pydantic
from pydantic import BaseModel, Field
from typing import Union, Optional

class EngineerData(BaseModel):
    CrawlerDate: str
    JobName: str
    JobEdu :str
    JobSal :Optional[int] 
    JobLoc :str
    JobCounty: str 
    JobExp :int  
    JobLanguage: str  
    JobKeyword: str   
    JobTool: str  
    JobToolCount: int  
    JobBusinessTrip: str
    JobUrl :str
    


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }
    

def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}