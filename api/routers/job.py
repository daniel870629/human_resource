from fastapi import APIRouter, Depends, HTTPException, status, Query, Path
from .. import schemas, main, database, models
from mongoengine.queryset.visitor import Q
from fastapi.encoders import jsonable_encoder
from bson import ObjectId
# Upgrade the FastAPI version to at least 0.95.1 before using Annotated.
from typing import Union, Annotated

# from ..crud import  crud_employee


router = APIRouter(
    prefix='/job',
    tags=['Job']
)

@router.get("/")
def read_root():
    return{'hello':'world'}

@router.get("/hello_world")
async def hello_world():
    return{'hello':'world'}

# 這邊的回應需要自己手動輸入
@router.get("/job_detail")
async def job_detail(JobKeyword: str):
    results = []
    print(JobKeyword.lower())
    async for result in database.engineer_collection.find({"JobKeyword": JobKeyword.lower()}):
        results.append(database.engineer_helper(result))
    return results

# ------------------------------------------------------------------------------------------------------
# Python 枚舉類型
# 這邊設計會有一點像是選單的效果
from enum import Enum
class ModelName(str, Enum):
    Python = "Python"
    Java = "Java"
    PHP = "PHP"


@router.get("/job_detail/{model_name}")
async def job_detail_enum(model_name: ModelName):
    results = []
    
    # 方法一
    if model_name is ModelName.Python:
        async for result in database.engineer_collection.find({"JobKeyword": model_name.lower()}):
            results.append(database.engineer_helper(result))
        return results
    
    # 方法二
    if model_name.value == "Java":
        async for result in database.engineer_collection.find({"JobKeyword": model_name.lower()}):
            results.append(database.engineer_helper(result))
        return results

    else:
        async for result in database.engineer_collection.find({"JobKeyword": model_name.lower()}):
            results.append(database.engineer_helper(result))
        return results  

# ------------------------------------------------------------------------------------------------------
# 情境：我只想找某一筆資料 限定在 地點：隨意也可以指定  薪水：自己輸入xxxxxx以上
# query parameters -> Optional parameters
# salary, a required int.
# skip, an int with a default value of 0.
# county, an optional str.
@router.get("/job_salary/{salary}")
async def read_item(salary: int, county: Union[str, None] = None,
                    # skip: int = 0
                    ):
    
    if salary <= 0:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail='ensure this value is greater than 0')

    results_list = []
    
    if county == None:
        async for result in database.engineer_collection.find( {'JobSal': {'$gt': salary}}):
            results_list.append(database.engineer_helper(result))    
    else:    
        async for result in database.engineer_collection.find( {'$and': [{'JobSal': {'$gt': salary}} , {'JobCounty': county}]}):
            results_list.append(database.engineer_helper(result))
        
    # print(results_list)
    return results_list

# ------------------------------------------------------------------------------------------------------

# 查詢參數和字符串校驗
@router.get('/Query_Parameters_and_String_Validations')
async def read_items(q:  Union[str, None] = Query(default=..., 
                                                  title="Query string", 
                                                  max_length=50, 
                                                  description="Query string for the items to search in the database that have a good match",)):
    # 使用省略號(...)聲明必需參數
    # Query(default=...) = Query(default=Required)
    
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results
# ------------------------------------------------------------------------------------------------------

# 查詢參數列表 / 多個值
# 情境：將使用者會的工具都填入，搜尋是否有匹配的工作內容，只要 list 裡面有符合的其中一個就回吐出來
@router.get('/Query_parameter_List_multiple_values')
async def multiple_values(tool: Union[list[str], None] = Query(default = None)):
    print(tool)
    results_list = []
    if not tool:
        async for result in database.engineer_collection.find():
            results_list.append(database.engineer_helper(result))
        return results_list
    else:    
        async for result in database.engineer_collection.find( {'JobTool': {'$in': tool}}):
            results_list.append(database.engineer_helper(result))
        return results_list
    
# ------------------------------------------------------------------------------------------------------

#  include_in_schema 參數設置為 False，表示在 API 文件中不顯示該參數。這樣做的目的是隱藏該參數，讓 API 的用戶無法看到它，以避免該參數被惡意使用。
@router.get("/include_in_schema/")
async def read_items(
    hidden_query: Union[str, None] = Query(default='asd', include_in_schema=False)
):
    if hidden_query:
        return {"hidden_query": hidden_query}
    else:
        return {"hidden_query": "Not found"}
    

# ------------------------------------------------------------------------------------------------------
# 情境：希望能縮小地區至xx區，而不是只有台北，台中 (嘗試使用 regex 參數)
@router.get("/regex_para")
async def regex_para(
    q: Union[str, None] = Query(
        default='大安區',
        description = '搜尋您想要的區域',
        regex = '^...$')
    ):
    results = []
    if q:
        async for result in database.engineer_collection.find({'JobLoc': {'$regex':q}}):
            results.append(database.engineer_helper(result))
    return results


# ------------------------------------------------------------------------------------------------------
# Alias_parameters
# item-query 這種參數是無法存在的，如果你真的想要使用這樣的形式作為參數，那就必須使用 alias 這樣的參數
# Deprecating parameters
# 你必須把它留在那裡一段時間，因為有客戶在使用它，但你希望文檔清楚地將它顯示為deprecated。

@router.get("/Deprecating_parameters/")
async def read_items(
    q: Union[str, None] = Query(
        # 一旦 default 有值，他就做為預設值，不管前面 Union 那邊
        default=None,
        alias="item-query",
        title="Query string",
        description="Query string for the items to search in the database that have a good match",
        min_length=3,
        max_length=50,
        regex="^fixedquery$",
        deprecated=True,
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results 

# ------------------------------------------------------------------------------------------------------

@router.get("/Import_Path/{item_id}")
async def read_items(
    item_id: Annotated[int, Path(title="The ID of the item to get")],
    q: Annotated[Union[str, None], Query(alias="item-query")] = None,
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results
# ------------------------------------------------------------------------------------------------------

# 643d4141fde26dad59e7f6c0
@router.put("/Body_Multiple_Parameters/{item_id}")
async def update_item(
    item_id: Annotated[str, Path(title="The ID of the item to get")],
    ):
    item_id = await database.engineer_collection.find_one({"_id": ObjectId(item_id)})
    if item_id:
        # 需要事先 from bson import ObjectId
        # item_id = await database.engineer_collection.update_one({"_id": ObjectId(item_id)},{'$set': {'JobCounty': '佛羅里達州'}})
        return('qqqq')
    else:
        HTTPException(status=status.HTTP_404_NOT_FOUND)
    
        return 'eee'
    
    
# HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail='ensure this value is greater than 0')