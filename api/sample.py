def add(a,b):
    return a+b

def add_one(a,b):
    result = add(a,b) + 1
    return result

def add_two(a,b):
    result = add(a,b) + 2
    return result