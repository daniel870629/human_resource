import os

MONGO_DATA_HOST = os.environ.get("MONGO_DATA_HOST", "127.0.0.1")
MONGO_DATA_USER = os.environ.get("MONGO_DATA_USER", "rootuser")
MONGO_DATA_PASSWORD = os.environ.get("MONGO_DATA_PASSWORD", "rootpass")
MONGO_DATA_PORT = int(os.environ.get("MONGO_DATA_PORT", "27017"))
MONGO_DATA_DATABASE = os.environ.get("MONGO_DATA_DATABASE", "jobdata")