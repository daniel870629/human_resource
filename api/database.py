import asyncio
import motor.core
import motor.motor_asyncio

from api import config

MONGO_DETAILS = f"mongodb://{config.MONGO_DATA_USER}:{config.MONGO_DATA_PASSWORD}@{config.MONGO_DATA_HOST}:{config.MONGO_DATA_PORT}/{config.MONGO_DATA_DATABASE}?authSource=admin&retryWrites=true&w=majority"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

client.get_io_loop = asyncio.get_running_loop

db = client.jobdata

engineer_collection = db.EngineerData

# 似乎用不到這個東西
# from pymongo import MongoClient

# def get_mongo_conn():
#     MONGO_DETAILS = (
#         f"mongodb://{config.MONGO_DATA_USER}:{config.MONGO_DATA_PASSWORD}"
#         f"@{config.MONGO_DATA_HOST}:{config.MONGO_DATA_PORT}/{config.MONGO_DATA_DATABASE}?authSource=admin&retryWrites=true&w=majority"
#     )
#     client = MongoClient(MONGO_DETAILS)
#     db = client.jobdata
#     engineerdata_collection = db.EngineerData
#     return engineerdata_collection


def engineer_helper(engineer) -> dict:
    return {
        'CrawlerDate': engineer["CrawlerDate"],
        "JobName": engineer["JobName"],
        'JobEdu': engineer["JobEdu"],
        "JobSal": engineer["JobSal"],
        'JobLoc': engineer["JobLoc"],
        "JobCounty": engineer["JobCounty"],
        'JobExp': engineer["JobExp"],
        'JobLanguage': engineer["JobLanguage"],
        'JobKeyword': engineer["JobKeyword"],
        'JobTool': engineer["JobTool"],
        'JobToolCount': engineer["JobToolCount"],
        'JobBusinessTrip': engineer["JobBusinessTrip"],
        "JobUrl": engineer["JobUrl"]
    }