import pytest
from api import sample

def test_add():
    expect = 3
    assert sample.add(1, 2) == expect
    
    
@pytest.fixture()
def some_data():
    raw_value = sample.add(1, 2)
    return raw_value

def test_add_one(some_data):
    expect = some_data + 1
    assert sample.add_one(1, 2) == expect
    
    
def test_add_two(some_data):
    expect = some_data + 2
    assert sample.add_two(1, 2) == expect
    
    
    