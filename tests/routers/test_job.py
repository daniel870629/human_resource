import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient
from api.main import app

client = TestClient(app)


# 同步的
def test_read_root():
    response = client.get('/job/')
    assert response.status_code == 200
    assert response.json() == {
        'hello': 'world'
    }

# 異步的 Pytest
@pytest.mark.asyncio
async def test_read_root_async():
    async with AsyncClient(app=app, base_url="http://127.0.0.1:8888") as ac:
        response = await ac.get('/job/hello_world')
    assert response.status_code == 200

# write unit test for methods job_detail
@pytest.mark.asyncio
async def test_job_detail():
    # for 異步
    async with AsyncClient(app=app, base_url="http://127.0.0.1:8888") as ac:
        response = await ac.get("/job/job_detail?JobKeyword=python")
        
    assert response.status_code == 200
    assert isinstance(response.json(), list)
    
@pytest.mark.parametrize('params',["Python","Java","PHP"])
# write unit test for methods job_detail_enum
@pytest.mark.asyncio
async def test_job_detail_enum(params):
    # for 異步
    async with AsyncClient(app=app, base_url="http://127.0.0.1:8888") as ac:
        response = await ac.get(f"/job/job_detail/{params}")
    assert response.json()[0]['JobKeyword'] == params.lower()
    assert response.status_code == 200
    assert isinstance(response.json(), list)